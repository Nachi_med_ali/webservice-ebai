var express  = require('express'),
    path     = require('path'),
    bodyParser = require('body-parser'),
    app = express(),
    expressValidator = require('express-validator');
var ebay = require('./scripts/Find.js').finde;

/*Set EJS template Engine*/
app.set('views','./views');
app.set('view engine','ejs');

app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: true })); //support x-www-form-urlencoded
app.use(bodyParser.json());
app.use(expressValidator());

/*MySql connection*/
var connection  = require('express-myconnection'),
    mysql = require('mysql');

app.use(

    connection(mysql,{
        host     : 'localhost',
        user     : 'root',
        password : '',
        database : 'comp',
        debug    : false //set true if you wanna see debug logger
    },'request')

);

app.get('/',function(req,res){
    res.send('Welcome');
});


//RESTful route
var router = express.Router();


/*------------------------------------------------------
*  This is router middleware,invoked everytime
*  we hit url /api and anything after /api
*  like /api/user , /api/user/7
*  we can use this for doing validation,authetication
*  for every route started with /api
--------------------------------------------------------*/
router.use(function(req, res, next) {
    console.log(req.method, req.url);
    next();
});

var ebaywebser = router.route('/item');


//show the CRUD interface | GET
ebaywebser.get(function(req,res,next){


    req.getConnection(function(err,conn){

        if (err) return next("Cannot Connect");

        var query = conn.query('SELECT * FROM hh_balrog_request',function(err,rows){

            if(err){
                console.log(err);
                return next("Mysql error, check your query");
            }

            res.render('search',{title:"Ebay Item Finder",data:rows});

         });

    });

});

//post data to DB | POST
ebaywebser.post(function(req,res,next){

    //validation
    req.assert('keyword','Keyword is required').notEmpty();

    var errors = req.validationErrors();
    if(errors){
        res.status(422).json(errors);
        return;
    }

    //get data
    var data = {
        name:req.body.keyword,
     };

    console.log(data);

    //inserting into mysql
    req.getConnection(function (err, conn){

        if (err) return next("Cannot Connect");

        var query = conn.query("SELECT * FROM hh_balrog_response WHERE request_id = (SELECT id from hh_balrog_request WHERE query_string = ? LIMIT 1) ", req.body.keyword,function(err,rows){

           if(err){
                console.log(err);
                return next("Mysql error, check your query");
           }
        console.log(rows);
            if(rows.length < 1 ){
                res.render('ebresult',{title : "No Result",req:req.body.keyword});
            } else {
            // var q = conn.query("SELECT * FROM hh_balrog_response WHERE request_id = (SELECT id from hh_balrog_request WHERE query_string = ? LIMIT 1) ", req.body.keyword,function(err,rows){

                res.render('tab',{data:rows});
        }
        });

     });

});


//now for Single route (GET,DELETE,PUT)
var ebaywebser2 = router.route('/item/:item_id');

/*------------------------------------------------------
route.all is extremely useful. you can use it to do
stuffs for specific routes. for example you need to do
a validation everytime route /api/user/:user_id it hit.

remove ebaywebser2.all() if you dont want it
------------------------------------------------------*/
ebaywebser2.all(function(req,res,next){
    console.log("You need to smth about ebaywebser2 Route ? Do it here");
    console.log(req.params);
    next();
});

//get data to update
ebaywebser2.get(function(req,res,next){

    var item_id = req.params.item_id;

    req.getConnection(function(err,conn){

        if (err) return next("Cannot Connect");

        var query = conn.query("SELECT * FROM hh_balrog_response WHERE request_id = ? ",[item_id],function(err,rows){

            if(err){
                console.log(err);
                return next("Mysql error, check your query");
            }

            //if user not found
            if(rows.length < 1)
                return res.render('result',{title : "No Result"});

            res.render('results',{title:"Results",data:rows});
        });

    });

});


//delete data
ebaywebser2.delete(function(req,res,next){

    var id = req.params.item_id;

     req.getConnection(function (err, conn) {

        if (err) return next("Cannot Connect");

        var query = conn.query("DELETE FROM hh_balrog_request WHERE id = ? ",req.params.item_id, function(err, rows){

             if(err){
                console.log(err);
                return next("Mysql error, check your query");
             }

             res.sendStatus(200);

        });
        //console.log(query.sql);

     });
});


var ebaykeyword = router.route('/keyword/');
ebaywebser2.all(function(req,res,next){
    console.log("You need to smth about ebaywebser2 Route ? Do it here");
    console.log(req.params);
    next();
});

//update data
ebaykeyword.put(function(req,res,next){
    console.log("inside keyword");
    var user_id = req.params.user_id;

    //validation
    req.assert('keyword','Keyword is required').notEmpty();

    var errors = req.validationErrors();
    if(errors){
        res.status(422).json(errors);
        return;
    }

    //get data
    var data_req = {
    source : '',
    process : 'ebay',
    country :  null,
    query_string : req.body.keyword,
    insert_daytime : new Date(),
    process_id : '',
    status : '9',
  }
    //inserting into mysql
    req.getConnection(function (err, conn){

        if (err) return next("Cannot Connect");

        var query = conn.query("INSERT INTO hh_balrog_request(source_id, processor_id, country, query_string, insert_datetime, processed_datetime, status ) VALUES ( ?, ?, ?, ?, ?, ?, ?)", [data_req.source, data_req.process, data_req.country, data_req.query_string, data_req.insert_daytime, data_req.process_id, data_req.status], function(err, rows){

           if(err){
                console.log(err);
                return next("Mysql error, check your query");
           }
           console.log(data_req);
        var ebayapi = ebay(data_req);

        });

     });

});




//now we need to apply our router here
app.use('/api', router);

//start Server
var server = app.listen(3000,function(){

   console.log("Listening to port %s",server.address().port);

});
