# EBAY Item Finder Webservice

## To run the script, simply run

* npm install && node server *

## Architecture

HTTP Format | Description | LINK
------------ | ------------- | -------------
GET | Returning all requests available in *hh_balrog_request* DB | /api/item/
POST | Send Keyword to test if item is available in DB, and if not return a view for EBAY search | /api/keyword/
GET | Get results of an item | /api/item/:id
PUT | Store keyword on DB, proceed to Ebay research and store results on hh_balrog_response | /api/keyword/:keyword
DELETE | DELETE an item | /api/item/:id